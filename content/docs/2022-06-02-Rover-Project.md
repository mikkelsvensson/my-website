+++
title =  "Rover Project"
date =   2022-06-02
+++
In our electronics course at the start of the 2. semester, we held a competition against our classmates and IT students from [Københavns Erhvervsakademi (KEA)](https://kea.dk/). 

![Picture of Rover](/assets/Rover.jpg)

During the competition, the rover needed to pick up a test tube under manual control. Then drive it to a walled track and enable an autopilot system. When reaching the end of the track the autopilot was disabled and the test tube was under manual control,   placed in a container. This had to be completed within 4 minutes. The group and I were the fastest and completed it with 1:20 left. Second place completed it with 0:50 left. It was ['a great day at KEA'](https://www.linkedin.com/posts/ilias-esmati-b354563b_a-great-day-at-kea-activity-6907353517754097664-RHua?utm_source=linkedin_share&utm_medium=member_desktop_web)

We were in groups of 3-5 students. Each group was tasked with constructing and writing the software for a Biohazard Removal Rover.

The rover was based on a PCB handed to each group because we did not have time to design it ourselves. Some components had to be soldered to the PCB.

Components:
 - 4 DC motors
 - 2 dual H-brigades
 - 4 22 µF capacitors
 - LDO regulator
 - Boost converter
 - 2 Ultrasonic sensor
 - Mosfet for buzzer
 - Buzzer
 - Female headers for esp32

The arm for the robot is the [EEZYbotArm](https://www.thingiverse.com/thing:1015238) from Thingiverse made by daGHZmo. The fingers on the robot were modified using Fusion360 to fit the test tube. The parts for the arm were 3D printed on Prusa Printers in Fablab at UCL.

The rover was controlled by an ESP32 microcontroller programmed in Micro Python. The esp32 hosted a webpage with controls for driving, arm movement and autopilot enable/disable.

The autopilot was a challenge to program. This was because of the ultrasonic sensors and their placement. When the rover turns the right-angled distance from the sensor to the wall will change even though the rover did not move closer to the wall.

All in all a great project with a lot of teamwork and a lot of learning.