+++
title =  "Temperature and humidity sensor for data centers"
date =   2022-06-04
+++
For our 2. semester project course we worked on temperature and humidity sensing for data centers.

![Sensor](/assets/HTP_sensor.jpg)

We worked with some 8. semester students from [Fredericia Maskinmesterskole](https://www.fms.dk). The students, Operational Maintenance Engineers (OME), had an elective course on data centers. 

Fredericia Maskinmesterskole has a data center with 6 racks. The OMEs can adjust the incoming temperature of the air to the racks but they do not know the exact temperature at every rack. That is what my project group made a solution for.

Using an ESP32 microcontroller and a BME280 temperature, humidity and pressure sensor and a Linux machine running a MQTT broker and webpage a system was made where it was possible to see the differences in temperature and humidity in the data center.

The system consists of two devices, the sensor(s) and the server.

A sensor is made up my an ESP32 developments board and a BME280 breakout board. The ESP32 is programmed in Micro Python. The two communicate using I2C. The ESP32 published the data it got from the BME280 to the MQTT broker.

The MQTT broker is running on an Azure virtual machine. This virtual machine is also running Node-Red with a dashboard. A Node-Red flow subscribes to the MQTT topic that the ESP32 published with. This data is then displayed on the dashboard and uploaded to a cloud MongoDB.

![Dashboard](/assets/HTP_Dashboard.jpg)

After completing the project, we presented the project to the OMEs. [Here is a response from one of them.](https://www.linkedin.com/posts/kresten-lykke-%C3%B8stergaard_datacenter-ittechnology-maskinmester-activity-6935192478790631425-rUSu?utm_source=linkedin_share&utm_medium=member_desktop_web)