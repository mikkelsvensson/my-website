+++
title =  "Digitising an old heat regulator"
date =   2022-11-20
+++
For our Industrial Application course we worked on digitising the outputs of an old heat regulator.

# The problem
The given task was as shown below:

An old heating system is used to teach regulation to students. With the current setup, a student has to measure the temperature manually through a voltage output on the system. There is a wish to upgrade the system so that the students no longer have to manually measure the temperature and potentially also extend the measurements to some of the other data that is available in the system, like flow and pressure.

The solution needs to include two parts, data collection and data availability.

Collecting the data comes with the challenge that the temperature is given in the voltage range of 0V – 10V. This means the solution needs to account for the higher voltage range that is not directly supported by microcontrollers. As the system is old, it is also a good idea to make sure that potential voltage spikes do not destroy the controller used.

Data availability in the context of this project means two things, GUI and OPC UA interface. The GUI should include the current values of the collected data in a way that is easily readable. The OPC UA interface should expose the collected data to OPC UA client.

# The solution
A RaspberryPi HAT (Hardware Attached *on* Top)
![A RaspberryPi HAT](/assets/ERI_HAT.jpg)
## The hardware
The 16-pin DIP package in the center of the PCB is a MCP3008. This is a 10-Bit Analogue to digital converter, and what the rest of the system is constructed around. 
### High input voltage
The first problem was that 10V is way above the maximum rating of the MCP3008. 
The ADC would also not be able to read a voltage higher than that of its reference. The reference used was the 5V output from the Raspberry Pi.
This problem was solved with a simple voltage divider consisting of two 1k resistors splitting the maximum voltage evenly down to 5V. 

As a first line of defence against any potential voltage spikes, some Zener diodes were placed in parallel with the resistor connected to ground. Zener diodes need a specific amount of current to break down at the right voltage. The datasheet specified the breakdown voltage at 5mA, hence the 1k resistors.
### Isolating the Pi
The Raspberry Pi is completely isolated from any of the inputs using an isolated DC-DC converter and four digital isolators. The four digital isolators transfer the SPI data used to communicate with the ADC. Optocouplers were also considered but with a rise and fall time in the low microseconds, they were just too slow. 
## The software
The Raspberry Pi runs an OPC UA server and exposes the data to any client of the same network. It also has a NodeJS webserver. This webserver subscribes to the OPC UA server and when a client makes a request to the webserver the data will be transferred using a websocket.

This project is available at [Gitlab](https://gitlab.com/industrial-applications/extended-regulator-interface)